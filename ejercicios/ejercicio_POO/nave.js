class Nave{
    constructor(){
        this.potencia = 50;
        this.coraza = 5;
    }

    get getPotencia(){
        return this.potencia;
    }

    get getCoraza(){
        return this.coraza;
    }

    encontrarPilaAtomica(){
        this.potencia += 25;
    }

    encontrarEscudo(){
        if((this.coraza+10)>20){
            this.coraza = 20;
        }
        else{
            this.coraza += 10;
        }
    }

    recibirAtaque(puntos){
        if(this.coraza>puntos){
            this.coraza -= puntos;
        }
        else if(this.coraza<puntos){
            puntos -= this.coraza;
            this.coraza = 0;
            this.potencia -= puntos;
        }
        else{
            if((this.potencia-puntos)<0){
                this.potencia = 0;
            }
            else{
                this.potencia -= puntos;
            }
        }
    }

    fortalezaDefensiva(){
        return (this.coraza+this.potencia);
    }

    necesitaFortalecerse(){
        if(this.coraza===0 && this.potencia<20){
            return true;
        }
        else{
            return false;
        }
    }

    fortalezaOfensiva(){
        if(this.potencia<20){
            return 0;
        }
        else{
            return ((this.potencia-20)/2);
        }
    }
}