class Contador{
    constructor(valor){
        this.valor = valor;
        this.comando = null;
    }

    get getValorActual(){
        return this.valor;
    }

    /**
     * @param {int} valor
     */
    set setValorActual(valor){
        this.valor = valor;
        this.comando = "actualizacion";
    }

    reset(){
        this.valor = 0;
        this.comando = "reset";
    }

    inc(){
        this.valor += 1;
        this.comando = "incremento";
    }

    dec(){
        this.valor -= 1;
        this.comando = "decremento";
    }

    ultimoComando(){
        return this.comando;
    }

}