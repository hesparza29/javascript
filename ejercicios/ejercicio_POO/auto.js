class Auto{
    constructor(){
        this.cambio = 0;
        this.rpm = 0;
        this.velocidad = 0;
    }

    arrancar(){
        this.cambio = 1;
        this.rpm = 500;
        this.velocidad = this.getVelocidad;
        this.consumo = 0.05;
    }

    subirCambio(){
        this.cambio++;
    }

    bajarCambio(){
        this.cambio--;
    }

    subirRPM(cuantos){
        this.rpm += cuantos;
    }

    bajarRPM(cuantos){
        this.rpm -= cuantos;
    }

    get getVelocidad(){
        this.velocidad = (this.rpm/100)*(0.5+(this.cambio/2));
        return this.velocidad;
    }

    consumoActualPorKm(){
        if(this.rpm>3000){
            this.consumo *= ((this.rpm-2500)/500);
        }
        if(this.cambio===1){
            this.consumo *= 3;
        }
        if(this.cambio===2){
            this.consumo *= 2;
        }
        return this.consumo;
    }

    
}