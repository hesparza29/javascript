class Telefono{
    constructor(){
        this.numero = "";
        this.valido = false;
        this.cantidadLlamadas = 0;
    }

    agregarDigito(digito){
        this.numero += digito;
    }

    llamar(){
        if(this.valido){
            console.log(`Llamando a ${this.numero}`);
            this.numero = "";
            this.valido = false;
            this.cantidadLlamadas +=1;
        }
        else{
            console.log(`El número ${this.numero} no es válido`);
        }
    }

    numeroIngresado(){
        console.log(`${this.numero}`);
    }

    esNumeroValido(){
        if(this.numero.length===5){
            this.valido = true;
        }
        else if(this.numero.length===7 && this.numero.substring(0,1)==="15"){
            this.valido = true;
        }
        else{
            this.valido = false;
        }
        
        console.log(this.valido);
    }

    borrarUltimoDigito(){
        this.numero = this.numero.substring(0, (this.numero.length-1));
    }

    cantLlamadas(){
        console.log(this.cantidadLlamadas);
    }
}