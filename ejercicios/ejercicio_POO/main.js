/**
 * Ejercicios propuestos de https://objetos1wollokunq.gitlab.io/material/guia1-objetos.pdf
 */

//Ejercicio 1 y Ejercicio 2
console.log("*****Ejercicio 1 y 2*****");
const contador = new Contador(0);

contador.setValorActual = 10;
contador.inc();
contador.inc();
contador.dec();
contador.inc();
console.log(contador.getValorActual);
console.log(contador.ultimoComando());

//Ejercicio 3
console.log("*****Ejercicio 3*****");
const pepita = new Pepita(0);

pepita.comer(100);
pepita.volar(10);
pepita.volar(20);
console.log(pepita.getEnergia);

//Ejercicio 4
console.log("*****Ejercicio 4*****");
const pepita2 = new Pepita(0);

pepita2.comer(85);
console.log(pepita2.cuantoQuiereVolar());

//Ejercicio 5
console.log("*****Ejercicio 5*****");
const calculadora = new Calculadora();

calculadora.cargar(0);
calculadora.sumar(4);
calculadora.multiplicar(5);
calculadora.restar(8);
calculadora.multiplicar(2);
console.log(calculadora.valorActual());

//Ejercicio 6
console.log("*****Ejercicio 6*****");
const telefono = new Telefono();

telefono.agregarDigito(8);
telefono.agregarDigito(9);
telefono.agregarDigito(0);
telefono.agregarDigito(2);
telefono.numeroIngresado();
telefono.esNumeroValido();
telefono.agregarDigito(3);
telefono.numeroIngresado();
telefono.esNumeroValido();
telefono.llamar();
telefono.agregarDigito(7);
telefono.agregarDigito(7);
telefono.numeroIngresado();
telefono.llamar();

//Ejercicio 7
console.log("*****Ejercicio 7*****");
const telefono2 = new Telefono();

telefono2.agregarDigito(8);
telefono2.agregarDigito(9);
telefono2.agregarDigito(0);
telefono2.agregarDigito(2);
telefono2.borrarUltimoDigito();
telefono2.numeroIngresado();
telefono2.cantLlamadas();

//Ejercicio 8
console.log("*****Ejercicio 8*****");

const nave = new Nave();

nave.encontrarPilaAtomica();
nave.recibirAtaque(14);
nave.encontrarEscudo();
console.log(nave.getPotencia, nave.getCoraza);

//Ejercicio 9
console.log("*****Ejercicio 9*****");

console.log(nave.fortalezaDefensiva());
console.log(nave.necesitaFortalecerse());
console.log(nave.fortalezaOfensiva());

//Ejercicio 10
console.log("*****Ejercicio 10*****");

const auto = new Auto();

auto.arrancar();
console.log(`${auto.velocidad} km/h`);
auto.subirRPM(4500);
console.log(`${auto.consumoActualPorKm()} litros/km`);
