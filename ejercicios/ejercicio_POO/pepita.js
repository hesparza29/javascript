class Pepita{
    constructor(energia){
        this.energia = energia;
        this.debil = true;
        this.feliz = false;
    }

    get getEnergia(){
        return this.energia;
    }

    comer(gramos){
        this.energia += gramos*4;
    }

    volar(kilometros){
        this.energia -= (kilometros+10);
    }

    estaDebil(){
        if(this.energia<50){
            this.debil = true;
        }
        else{
            this.debil = false;
        }
    }

    estaFeliz(){
        if(this.energia>500 && this.energia<1000){
            this.feliz = true;
        }
        else{
            this.feliz = false;
        }
    }

    cuantoQuiereVolar(){
        let kilometros = 0;
        kilometros = this.energia/5;
        
        if(this.energia>300 && this.energia<400){
            kilometros += 10;
        }
        if(this.energia%20===0){
            kilometros += 15;
        }

        return kilometros;
    }
}