import {invierte_palabras} from "./invierte_palabras.js";
import {contador_de_palabras} from "./contar_palabras_en_texto.js";
import {validaPalindromo} from "./validar_si_cadena_es_palindromo.js";
import {eliminaPatron} from "./eliminar_patron_de_cadena.js";

console.log(invierte_palabras("Hola Mundo"));
console.log(contador_de_palabras("hola mundo adios mundo", "mundo"));
console.log(validaPalindromo("Salas"));
console.log(eliminaPatron("xyz1, xyz2, xyz3, xyz4 y xyz5", "xyz"));