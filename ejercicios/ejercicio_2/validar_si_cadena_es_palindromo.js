/**
 * 7) Programa una función que valide si 
 * una palabra o frase dada, es un palíndromo 
 * (que se lee igual en un sentido que en otro), 
 * pe. mifuncion("Salas") devolverá true.
 */

export const validaPalindromo = (cadena = "") => {
    let validacion;
    if((typeof cadena)==="string"){
        let palindromo = "";
        cadena = cadena.toLowerCase();
        cadena = cadena.replace(/ /g, "");
        for(let i = (cadena.length-1); i >= 0; i--){
            palindromo += cadena[i];
        }
        (cadena===palindromo) ? validacion = true : validacion = false;
    }
    else{
        validacion = "No es una cadena";
    }
    return validacion;
}