/**
 * 8) Programa una función que elimine 
 * cierto patrón de caracteres de un texto dado, 
 * pe. miFuncion("xyz1, xyz2, xyz3, xyz4 y xyz5", "xyz") 
 * devolverá  "1, 2, 3, 4 y 5.
 */

export const eliminaPatron = (cadena = "", patron = "")=>{
    let nueva_cadena = "";
    const expReg = new RegExp(patron, "g");
    if((typeof cadena)==="string"){
        nueva_cadena = cadena.replace(expReg, "");
    }
    else{
        nueva_cadena = "No es una cadena";
    }

    return nueva_cadena;
}