/**
 * 6) Programa una función para contar 
 * el número de veces que se repite una 
 * palabra en un texto largo, 
 * pe. miFuncion("hola mundo adios mundo", "mundo") devolverá 2.
 */

export function contador_de_palabras(cadena, palabra){
    let coincidencias = 0;
    if((typeof cadena)==="string"){
        let textos = cadena.split(" ");
        for(let i = 0; i < textos.length; i++){
            (palabra===textos[i]) ? coincidencias++:false;    
        }
    }   
    else{
        coincidencias = "No es una cadena";
    }
    return coincidencias;
}