/**
 * 5) Programa una función que invierta 
 * las palabras de una cadena de texto, 
 * pe. miFuncion("Hola Mundo") devolverá "odnuM aloH".
 */

export function invierte_palabras(cadena){
    let cadena_invertida = "";
    if((typeof cadena)==="string"){
        for(let i = ((cadena.length)-1); i >= 0; i--){
            cadena_invertida += cadena[i];
        }
    }
    else{
        cadena_invertida = "No es una cadena";
    }
    return cadena_invertida;
}