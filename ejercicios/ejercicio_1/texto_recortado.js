/**
 * 2) Programa una función que te devuelva el texto recortado 
 * según el número de caracteres indicados, 
 * pe. miFuncion("Hola Mundo", 4) devolverá "Hola".
 */

export function texto_recortado(cadena, separador){
    let texto_recortado = "";
    if((typeof cadena)==="string"){
        for (let i = 0; i < separador; i++){
            texto_recortado += cadena[i]; 
        }
    }
    else{
        texto_recortado = "No es una cadena";
    }
    
    return texto_recortado;
}