/**
 * 3) Programa una función que dada una String 
 * te devuelva un Array de textos separados por 
 * cierto caracter, pe. miFuncion('hola que tal', ' ') 
 * devolverá ['hola', 'que', 'tal'].
 */

export function separa_texto(cadena, separador){
    let textos = [];
    let valor = "";

    if((typeof cadena)==="string"){
        cadena += separador;
        for(let i = 0; i < cadena.length; i++){
            if(cadena[i]!=separador){
                valor += cadena[i];
            }
            else{
                textos.push(valor);
                valor = "";
            }
        }
        return textos;
    }
    else{
        return "No es una cadena";
    }
}