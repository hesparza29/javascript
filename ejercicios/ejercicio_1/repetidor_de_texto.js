/**
 * 4) Programa una función que repita un texto 
 * X veces, pe. miFuncion('Hola Mundo', 3) 
 * devolverá Hola Mundo Hola Mundo Hola Mundo.
 */

export function repite_texto(cadena, numero){
    let texto = "";
    for (let i = 0; i < numero; i++) {
        texto += cadena + " ";
    }
    return texto;
}