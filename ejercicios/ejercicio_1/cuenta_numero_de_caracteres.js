/**
 * 1) Programa una función que cuente el número de caracteres 
 * de una cadena de texto, pe. miFuncion("Hola Mundo") 
 * devolverá 10.
 */
export function cuenta_caracteres(cadena){
    let tamanio = 0;
    if((typeof cadena)==="string"){
        while(cadena[tamanio]!=undefined){
            tamanio++;
        }
    }
    else{
        tamanio = "No es cadena";
    }
    return tamanio;
}