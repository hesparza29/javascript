/**
 * Ejercicio 1 https://youtu.be/3o5d-tdVscU
 */

import {cuenta_caracteres} from "./cuenta_numero_de_caracteres.js";
import {texto_recortado} from "./texto_recortado.js";
import {separa_texto} from "./separa_texto_en_arreglo.js"
import {repite_texto} from "./repetidor_de_texto.js"

console.log(cuenta_caracteres("Hola Mundo"));
console.log(texto_recortado("Hola Mundo", 4));
console.log(separa_texto("hola que tal", " "));
console.log(repite_texto("Hola Mundo", 3));