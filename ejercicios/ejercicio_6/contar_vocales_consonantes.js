/**
 * 18) Programa una función que dada una cadena 
 * de texto cuente el número de vocales y consonantes, 
 * pe. miFuncion("Hola Mundo") devuelva Vocales: 4, Consonantes: 5.
 */

export const contarVocalesConsonantes = (cadena = "") =>{
    let resultado;

    switch(true){
        case (cadena==="" || cadena===undefined):
            resultado = `Vocales: 0, Consonantes: 0`;
            break;
        
        case (cadena.length>0):
            const vocal = ["a", "e", "i", "o", "u"];
            const consonante = ["b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", 
                                "p", "q", "r", "s", "t", "v", "w", "x", "y", "z"];
            let numeroDeVocales = 0;
            let numeroDeConsonantes = 0;
            cadena = cadena.toLowerCase();

            for (let i = 0; i < cadena.length; i++){
                (vocal.includes(cadena[i])) ? numeroDeVocales++ : (consonante.includes(cadena[i])) ? numeroDeConsonantes++ : false;
            }
            resultado = `Vocales: ${numeroDeVocales}, Consonantes: ${numeroDeConsonantes}`;
            break;
        default:
            break;
    }

    return resultado;
};