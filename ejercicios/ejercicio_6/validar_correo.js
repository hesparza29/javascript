/**
 * 20) Programa una función que valide 
 * que un texto sea un email válido, 
 * pe. miFuncion("jonmircha@gmail.com") devolverá verdadero.
 */

export const validarCorreo = (correo="")=>{
    let validacion;
    let expReg = /[a-z]*@[a-z]*.[a-z]*/;
    (expReg.test(correo)) ? validacion = true : validacion = false;
    
    return validacion;
}