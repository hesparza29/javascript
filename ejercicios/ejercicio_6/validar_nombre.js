import {contarVocalesConsonantes} from "./contar_vocales_consonantes.js";
/**
 * 19) Programa una función que valide 
 * que un texto sea un nombre válido, 
 * pe. miFuncion("Jonathan MirCha") devolverá verdadero.
 */

export const validarNombre = (nombreCompleto = "") =>{
    let validacion;

    switch(true){
        case ((typeof nombreCompleto)==="string" && nombreCompleto.length>0):
            nombreCompleto = nombreCompleto.split(" ");
            let nombre = nombreCompleto[0];
            let apellido = nombreCompleto[1];
            let vocalesNombre = 0;
            let consonantesNombre = 0;
            let vocalesApellido = 0;
            let consonantesApellido = 0;
            
            nombre = contarVocalesConsonantes(nombre);
            apellido = contarVocalesConsonantes(apellido);
            while(nombre.indexOf(" ", 0)!==-1 && apellido.indexOf(" ", 0)){
                nombre = nombre.replace(" ", "");
                apellido = apellido.replace(" ", "");
            }
            nombre = nombre.split(",");
            vocalesNombre = nombre[0].split(":");
            vocalesNombre = parseInt(vocalesNombre[1]);
            consonantesNombre = nombre[1].split(":");
            consonantesNombre = parseInt(consonantesNombre[1]);
            apellido = apellido.split(",");
            vocalesApellido = apellido[0].split(":");
            vocalesApellido = parseInt(vocalesApellido[1]);
            consonantesApellido = apellido[1].split(":");
            consonantesApellido = parseInt(consonantesApellido[1]);

            (vocalesNombre>0 && consonantesNombre>1 && vocalesApellido>1 && consonantesApellido>1) ? 
                validacion = true : validacion = false;
    
            break;
        case ((typeof nombreCompleto)!=="string"):
            validacion = "No ingresaste una cadena de texto";
            break;

        case (nombreCompleto.length===0 || nombreCompleto===undefined):
            validacion = "No ingresaste ningún valor";
            break;
        default:
            break;
    }

    return validacion;
};