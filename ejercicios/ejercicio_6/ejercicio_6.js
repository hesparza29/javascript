import {contarVocalesConsonantes} from "./contar_vocales_consonantes.js";
import {validarNombre} from "./validar_nombre.js";
import {validarCorreo} from "./validar_correo.js";

console.log(contarVocalesConsonantes("Hola Mundo"));
console.log(validarNombre("Jonathan MirCha"));
console.log(validarCorreo("jonmircha@gmail.com"));