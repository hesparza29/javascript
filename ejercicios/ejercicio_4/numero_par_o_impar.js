/**
 * 13) Programa una función que determine 
 * si un número es par o impar, 
 * pe. miFuncion(29) devolverá Impar.
 */
export const parOImpar = (numero = 0)=>{
    let estatus;
    switch((typeof numero)){
        case "number":
            (numero%2===0) ? estatus="Par" : estatus="Impar";
            break;
    
        default:
            estatus = "No ingresaste un número";
            break;
    }
    return estatus;
}