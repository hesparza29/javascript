/**
 * 14) Programa una función para convertir 
 * grados Celsius a Fahrenheit y viceversa, 
 * pe. miFuncion(0,"C") devolverá 32°F.
 */

export const convertirGrados = (grados, opcion = undefined) => {
    let conversion;
    switch(opcion){
        case "C":
            ((typeof grados)==="number") ? conversion = grados*1.8+32 : conversion = "No ingresaste un valor númerico";
            break;

        case "F":
            ((typeof grados)==="number") ? conversion = (grados-32)/1.8 : conversion = "No ingresaste un valor númerico";
            break;
        
        case undefined:
            conversion = "No ingresaste ninguna opción";
            break;
    
        default:
            conversion = "Seleccionaste una opción incorrecta";
            break;
    }

    return conversion;
}