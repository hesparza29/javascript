/**
 * 12) Programa una función que determine 
 * si un número es primo (aquel que solo es 
 * divisible por sí mismo y 1) o no, pe. miFuncion(7) devolverá true.
 */

export const esPrimo = numero => {
    let primo;
    if((typeof numero)==="number"){
        switch(true){
            case (numero<=0):
                primo = "Introduce un número mayor a 0";
                break;
            case (numero===1):
                primo = true;
                break;
            case (numero>1):
                let divisores = 0;
                for(let i = 1; i <= numero; i++){
                    (numero%i===0)?divisores++:false;
                }
                (divisores===2)?primo=true:primo=false;
                break;
        }
    }
    else{
        primo = "No es un número";
    }

    return primo;
}