import {esPrimo} from "./numero_primo.js";
import {parOImpar} from "./numero_par_o_impar.js";
import {convertirGrados} from "./convertir_grados.js";

console.log(esPrimo(25));
console.log(parOImpar("Hola"));
console.log(convertirGrados(35, "F"));