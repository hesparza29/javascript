import {elevarAlCuadrado} from "./numeros_al_cuadrado.js";
import {numeroMasAltoYMasBajo} from "./numero_mas_alto_y_mas_bajo.js";
import {paresEImpares} from "./numeros_pares_e_impares.js";

console.log(elevarAlCuadrado([1, 4, 5]));
console.log(numeroMasAltoYMasBajo([1, 4, 5, 99, -60]));
console.log(paresEImpares([1,2,3,4,5,6,7,8,9,0]));