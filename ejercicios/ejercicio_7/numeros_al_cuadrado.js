/**
 * 21) Programa una función que dado un array
 * numérico devuelve otro array con los números 
 * elevados al cuadrado, pe. mi_funcion([1, 4, 5]) 
 * devolverá [1, 16, 25].
 */

export const elevarAlCuadrado = (numeros = []) => {
    let numerosCuadrados;

    switch(true){
        case ((numeros instanceof Array) && numeros.length>0):
            numerosCuadrados = [];
            numeros.forEach(elemento => {
                (typeof elemento==="number") 
                ? numerosCuadrados.push(Math.pow(elemento, 2)) 
                : numerosCuadrados.push("No es un valor númerico");
            });
            break;
        case (!(numeros instanceof Array)):
            numerosCuadrados = "No ingresaste un arreglo";
            break;

        case ((numeros instanceof Array) && numeros.length===0):
            numerosCuadrados = "Ingresaste un arreglo vació";
            break;
        default:
            break;
    }

    return numerosCuadrados;
};