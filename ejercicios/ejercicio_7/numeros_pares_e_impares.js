/**
 * 23) Programa una función que dado 
 * un array de números devuelva un objeto 
 * con 2 arreglos en el primero almacena los 
 * números pares y en el segundo los impares, 
 * pe. miFuncion([1,2,3,4,5,6,7,8,9,0]) devolverá {pares: [2,4,6,8,0], impares: [1,3,5,7,9]}.
 */

export const paresEImpares = (numeros = []) => {
    let resultado;

    switch(true){
        case ((numeros instanceof Array) && numeros.length>0):
            let pares = [];
            let impares = [];
            for(let i = 0;  i<numeros.length ; i++){
                ((typeof numeros[i])==="number") 
                ? (numeros[i]%2===0) 
                    ? pares.push(numeros[i]) 
                    : impares.push(numeros[i])
                : false;
            }
            resultado = {
                pares,
                impares
            };
            break;
        
        case ((numeros instanceof Array) && numeros.length===0):
            resultado = "Ingresaste un arreglo vacio.";
            break;
    
        default:
            resultado = "No ingresaste un arreglo.";
            break;
    }

    return resultado;
};