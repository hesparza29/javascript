import {ordenamientoPorSeleccion} from "./../../ordenamiento_por_seleccion.js";

/**
 * 22) Programa una función que dado un array 
 * devuelva el número mas alto y el más bajo de dicho array, 
 * pe. miFuncion([1, 4, 5, 99, -60]) devolverá [99, -60].
 */

export const numeroMasAltoYMasBajo = (numeros = []) => {

  let resultado;

  switch(true){
    case ((numeros instanceof Array) && numeros.length>0):
      ordenamientoPorSeleccion(numeros);
      resultado = [numeros[numeros.length-1], numeros[0]];
      break;
    
    case ((numeros instanceof Array) && numeros.length===0):
      resultado = "Ingresaste un arreglo vacio.";
      break;

    default:
      resultado = "No ingresaste un arreglo.";
      break;
  }

  return resultado;
};