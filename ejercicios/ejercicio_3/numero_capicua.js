/**
 * 10) Programa una función que reciba un 
 * número y evalúe si es capicúa o no 
 * (que se lee igual en un sentido que en otro), 
 * pe. miFuncion(2002) devolverá true.
 */

export const numero_capicua = numero =>{
    numero = numero.toString();
    let numeroAlReves = "";
    let comprobacion = false;

    for(let i = (numero.length-1); i>=0 ; i--){
        numeroAlReves += numero[i];
    }

    if(numeroAlReves===numero){
        comprobacion = true;
    }

    return comprobacion;

}