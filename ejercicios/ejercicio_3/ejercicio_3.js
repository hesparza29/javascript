import {generar_numero_aleatorio} from "./generar_numero_aleatorio.js";
import {numero_capicua} from "./numero_capicua.js";
import {factorial} from "./factorial.js";

console.log(generar_numero_aleatorio());
console.log(numero_capicua(859));
console.log(factorial(5));