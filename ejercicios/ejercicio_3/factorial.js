/**
 * 11) Programa una función que calcule 
 * el factorial de un número 
 * (El factorial de un entero positivo n, se define como el 
 * producto de todos los números enteros positivos desde 1 hasta n), 
 * pe. miFuncion(5) devolverá 120.
 */

export const factorial = numero => {
    let factorial;

    if((numero%1)===0){
        factorial = 1;
        while(numero>0){
            factorial *= numero;
            numero--;
        }
    }
    else{
        factorial = "No ingresaste un número entero";
    }

    return factorial;
}