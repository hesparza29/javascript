/**
 * 25) Programa una función que dado 
 * un arreglo de elementos, elimine los duplicados, 
 * pe. miFuncion(["x", 10, "x", 2, "10", 10, true, true]) devolverá ["x", 10, 2, "10", true].
 */

export const eliminarDuplicados = (arreglo = []) => {
    let resultado;
    
    switch(true){
        case ((arreglo instanceof Array) && arreglo.length>0):
            resultado = [];
            for(let i = 0; i < arreglo.length; i++){
                (!(resultado.includes(arreglo[i]))) ? resultado.push(arreglo[i]) : false;
            }
            break;
        
        case ((arreglo instanceof Array) && arreglo.length===0):
            resultado = "Ingresaste un arreglo vacio.";
            break;

        default:
            resultado = "No ingresaste un arreglo.";
            break;
    }

    return resultado;
};