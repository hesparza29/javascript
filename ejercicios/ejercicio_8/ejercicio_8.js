import {ordenAscendenteDescendente} from "./arreglos_ascendente_y_descendente.js";
import {eliminarDuplicados} from "./eliminar_duplicados_de_arreglo.js";
import {promedio} from "./arreglos_sacar_promedio.js";

console.log(ordenAscendenteDescendente([7,5,7,8,6]));
console.log(eliminarDuplicados(["x", 10, "x", 2, "10", 10, true, true]));
console.log(promedio([9,8,7,6,5,4,3,2,1,0]));