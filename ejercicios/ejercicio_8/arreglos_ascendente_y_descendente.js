import {ordenamientoPorSeleccion} from "../../ordenamiento_por_seleccion.js";
/**
 * 24) Programa una función que dado un arreglo 
 * de números devuelva un objeto con dos arreglos, 
 * el primero tendrá los numeros ordenados en 
 * forma ascendente y el segundo de forma descendiente, 
 * pe. miFuncion([7,5,7,8,6]) devolverá { asc: [5,6,7,7,8], desc: [8,7,7,6,5] }.
 */

export const ordenAscendenteDescendente = (numeros = []) => {
    let resultado;
    
    switch(true){
        case ((numeros instanceof Array) && numeros.length>0):
        let asc = [], desc = [], contador = 0;
        asc = ordenamientoPorSeleccion(numeros);
        for(let i = asc.length-1; i >= 0; i--) {
            desc[contador] = asc[i];
            contador++;
        }
        resultado = {
            asc,
            desc
        };
        break;
        
        case ((numeros instanceof Array) && numeros.length===0):
        resultado = "Ingresaste un arreglo vacio.";
        break;

        default:
        resultado = "No ingresaste un arreglo.";
        break;
    }

    return resultado;
};