/**
 * 26) Programa una función que dado un 
 * arreglo de números obtenga el promedio, 
 * pe. promedio([9,8,7,6,5,4,3,2,1,0]) devolverá 4.5.
 */

 export const promedio = (arreglo = []) => {
    let resultado;
    
    switch(true){
        case ((arreglo instanceof Array) && arreglo.length>0):
            resultado = 0;
            let contador = arreglo.length;
            for(let i = 0; i < arreglo.length; i++){
                (typeof arreglo[i]==="number") ? resultado += arreglo[i] : contador--;
            }
            resultado /= contador;
            break;
        
        case ((arreglo instanceof Array) && arreglo.length===0):
            resultado = "Ingresaste un arreglo vacio.";
            break;

        default:
            resultado = "No ingresaste un arreglo.";
            break;
    }

    return resultado;
};