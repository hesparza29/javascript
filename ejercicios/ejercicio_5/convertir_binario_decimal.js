import {invierte_palabras} from "../ejercicio_2/invierte_palabras.js";
/**
 * 15) Programa una función para convertir 
 * números de base binaria a decimal 
 * y viceversa, pe. miFuncion(100,2) 
 * devolverá 4 base 10.
 */

export const convercionNumerica = (numero = undefined, base = undefined) => {
    let conversion = "";

    switch(true){
        case (numero===undefined && base===undefined):
            conversion = "Introduce un valor númerico y la base de la conversión";
            break;
        
        case ((typeof numero)==="number" && (typeof base)==="number"):
            if(base===2){
                while(numero>0){
                    conversion += (numero%2).toString();
                    numero = parseInt(numero/2);
                }
                conversion = invierte_palabras(conversion);
            }
            else if(base===10){
                conversion = 0;
                numero = invierte_palabras(numero.toString());
                for (let i = 0; i < numero.length; i++){
                    (numero[i]==="1") ? conversion += Math.pow(i, 2) : false;
                }
            }
            else{
                conversion = "No seleccionaste una base válida, solamente se permite el valor 2 y 10";
            }
            break;
        default:
            conversion = "No introduciste valores númericos";
            break;
    }

    return conversion;
};