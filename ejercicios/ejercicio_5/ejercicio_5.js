import {convercionNumerica} from "./convertir_binario_decimal.js";
import {aplicarDescuento} from "./aplicar_descuento.js";
import {tiempoTranscurrido} from "./tiempo_transcurrido.js";

console.log(convercionNumerica(11010, 2));
console.log(aplicarDescuento(7500, 69.610));
console.log(tiempoTranscurrido(1984,4,23));