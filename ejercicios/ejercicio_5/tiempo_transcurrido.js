/**
 * 17) Programa una función que dada una 
 * fecha válida determine cuantos años han 
 * pasado hasta el día de hoy, pe. miFuncion(new Date(1984,4,23)) 
 * devolverá 35 años (en 2020).
 */


export const tiempoTranscurrido = (anio = 1970, mes = 0, dia = 1) => {
    let fecha = new Date(anio, mes, dia);
    let fechaHoy = new Date();
    let milisegundosEnUnAnio = 31556900000;
    let aniosTranscurridos;
    (fecha!=="Invalid Date") ? 
        aniosTranscurridos = parseInt(((fechaHoy.getTime())-(fecha.getTime()))/milisegundosEnUnAnio) : 
        aniosTranscurridos = "Introduciste una fecha inválida";

    return aniosTranscurridos;
    
};