/**
 * 16) Programa una función que devuelva el monto 
 * final después de aplicar un descuento a una 
 * cantidad dada, pe. miFuncion(1000, 20) devolverá 800.
 */


export const aplicarDescuento = (cantidad = undefined, descuento = undefined) => {
    
    let montoFinal;

    switch(true){
        case ((typeof cantidad)==="number" && (typeof descuento)==="number" && cantidad>0):
            (descuento>0 && descuento<100) ? 
                montoFinal = (cantidad-(cantidad*(descuento/100))): 
                montoFinal = "El descuento aplicado debe de ser mayor a 0 y menor a 100";
            break;
    
        default:
            montoFinal = "No introduciste valores númericos, además la cantidad debe de ser mayor a 0";
            break;
    }

    return montoFinal;
}