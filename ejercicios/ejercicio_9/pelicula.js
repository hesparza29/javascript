/**
 * 27) Programa una clase llamada Pelicula.

La clase recibirá un objeto al momento de instanciarse con los siguentes datos: 
id de la película en IMDB, titulo, director, año de estreno, país o países de origen, géneros y calificación en IMBD.
  - Crea un método estático que devuelva los géneros aceptados*.
  - Crea un método que devuelva toda la ficha técnica de la película.
  - Apartir de un arreglo con la información de 3 películas genera 3 
    instancias de la clase de forma automatizada e imprime la ficha técnica 
    de cada película.

* Géneros Aceptados: Action, Adult, Adventure, Animation, Biography, Comedy, 
Crime, Documentary ,Drama, Family, Fantasy, Film Noir, Game-Show, History, 
Horror, Musical, Music, Mystery, News, Reality-TV, Romance, Sci-Fi, Short, 
Sport, Talk-Show, Thriller, War, Western.
 */

export class Pelicula{
    constructor(objeto){
        switch(true){
            /**
             * Valida que el id IMDB tenga 9 caracteres, 
             * los primeros 2 sean letras y los 7 restantes números.
             */
            case (!(this.validarIMDB(objeto.IMDB))):
                console.error(`El IMDB debe tener 9 caracteres, los primeros 2 deben ser letras y los 7 restantes números.`);
                this.IMDB = null;
                this.titulo = null;
                this.director = null;
                this.estreno = null;
                this.paises = null;
                this.generos = null;
                this.calificacion = null;
                break;
            /**
             * Valida que el título no rebase los 100 caracteres.
             */
            case (!(this.validarTitulo(objeto.titulo))):
                console.error(`El titulo no debe de rebasar los 100 caracteres y debe de contener minimo 2 caracteres, 
                solamente se permiten letras del alfabeto latino y solamente se pueden usar los carcateres especiales "-" y "."`);
                this.IMDB = null;
                this.titulo = null;
                this.director = null;
                this.estreno = null;
                this.paises = null;
                this.generos = null;
                this.calificacion = null;
                break;
            /**
             * Valida que el director no rebase los 50 caracteres.
             */
            case (!(this.validarTitulo(objeto.director))):
                console.error(`El nombre del director no debe de rebasar los 50 caracteres y debe de contener minimo 5 caracteres, 
                solamente se permiten letras del alfabeto latino.`);
                this.IMDB = null;
                this.titulo = null;
                this.director = null;
                this.estreno = null;
                this.paises = null;
                this.generos = null;
                this.calificacion = null;
                break;
            /**
             * Valida que el año de estreno sea un número entero de 4 dígitos.
             */
            case (!(this.validarAnioEstreno(objeto.estreno))):
                console.error(`El año de estreno debe de ser un número entero entre 1898 y el año actual.`);
                this.IMDB = null;
                this.titulo = null;
                this.director = null;
                this.estreno = null;
                this.paises = null;
                this.generos = null;
                this.calificacion = null;
                break;
            /**
             * Valida que el país o paises sea introducidos en forma de arreglo.
             */
            case (!(this.validarPaises(objeto.paises))):
                console.error(`El país o los paises de estreno deben de ser introducidos en forma de arreglo, 
                ademas debe de contener al menos 1 valor.`);
                this.IMDB = null;
                this.titulo = null;
                this.director = null;
                this.estreno = null;
                this.paises = null;
                this.generos = null;
                this.calificacion = null;
                break;
            /**
             * Valida que los géneros sean introducidos en forma de arreglo.
             * Valida que los géneros introducidos esten dentro de los géneros aceptados.
             */
            case (!(this.validarGenerosAceptados(objeto.generos))):
                console.error(`Solamente se permiten los generos: 
                ${Pelicula.generosAceptados()}`);
                this.IMDB = null;
                this.titulo = null;
                this.director = null;
                this.estreno = null;
                this.paises = null;
                this.generos = null;
                this.calificacion = null;
                break;
            /**
             * Valida que la calificación sea un número entre 0 y 10 
             * pudiendo ser decimal de una posición.
             */
            case (!(this.validarCalificacion(objeto.calificacion))):
                console.error(`Solamente se permite una calificación de 0.0 a 10.0`);
                this.IMDB = null;
                this.titulo = null;
                this.director = null;
                this.estreno = null;
                this.paises = null;
                this.generos = null;
                this.calificacion = null;
                break;
            /**
             * Despues de cumplir con todas las validaciones anteriores y 
             * verificar que los valores no vengan vacios 
             * se crea el objeto.
             */
            default:
                this.IMDB = objeto.IMDB;
                this.titulo = objeto.titulo;
                this.director = objeto.director;
                this.estreno = objeto.estreno;
                this.paises = objeto.paises;
                this.generos = objeto.generos;
                this.calificacion = parseFloat(objeto.calificacion.toFixed(1));
                break;
        }
    }

    validarIMDB(IMDB){
        let expRegIMDB = /^([a-z\u00f1áéíóúü]){2,2}([0-9]){7,7}$/;
        let validacion = false;
        (expRegIMDB.test(IMDB.toLowerCase())) ? validacion = true: validacion = false;
        return validacion;
    }

    validarTitulo(titulo){
        let expRegTitulo = /^([a-z\u00f1áéíóúü -.]){2,100}$/;
        let validacion = false;
        (expRegTitulo.test(titulo.toLowerCase())) ? validacion = true: validacion = false;
        return validacion;
    }

    validarDirector(director){
        let expRegDirector = /^([a-z\u00f1áéíóúü ]){5,50}$/;
        let validacion = false;
        (expRegDirector.test(director.toLowerCase())) ? validacion = true: validacion = false;
        return validacion;
    }

    validarAnioEstreno(estreno){
        let validacion = false;
        let fecha = new Date();
        ((typeof estreno==="number") && (estreno>=1895) && (estreno<=fecha.getFullYear())) 
            ? validacion = true 
            : validacion = false;
        return validacion;
    }

    validarPaises(paises){
        let validacion = false;
        (paises instanceof Array && paises.length>0) ? validacion = true : validacion = false;
        return validacion;
    }

    validarGeneros(generos){
        let validacion = false;
        (generos instanceof Array) ? validacion = true : validacion = false;
        return validacion;
    }

    validarGenerosAceptados(generos){
        let validacion = true;
        const generosAceptados = Pelicula.generosAceptados();
        for(let i = 0; i < generos.length; i++){
            if(!(generosAceptados.includes(generos[i]))){
                validacion = false;
                break;
            }
            
        }
        return validacion;
    }

    static generosAceptados(){
        const generos = [
            "Action", "Adult", "Adventure", "Animation", "Biography", 
            "Comedy", "Crime", "Documentary", "Drama", "Family", 
            "Fantasy", "Film Noir", "Game-Show", "History", "Horror", 
            "Musical", "Music", "Mystery", "News", "Reality-TV", 
            "Romance", "Sci-Fi", "Short", "Sport", "Talk-Show", 
            "Thriller", "War", "Western"
        ];

        return generos;
    }

    validarCalificacion(calificacion){
        let validacion = false;
        ((typeof calificacion==="number") && calificacion>=0 && calificacion<=10) 
        ? validacion = true 
        : validacion = false;
        return validacion;
    }

    consultarInformacionPelicula(){
        return `        ID de película: ${this.IMDB} 
        Titulo: ${this.titulo} 
        Director: ${this.director} 
        Año de estreno: ${this.estreno} 
        País o Países de origen: ${this.paises} 
        Géneros: ${this.generos} 
        Calificación: ${this.calificacion}`;
    }
}