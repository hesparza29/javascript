import {Pelicula} from "./pelicula.js";

const misPeliculas = [
    {
        IMDB: "ae4587965",
        titulo: "El Rey León",
        director: "Rob Minkoff",
        estreno: 1994,
        paises: ["Estados Unidos"],
        generos: ["Animation", "Musical"],
        calificacion: 8.45
    },
    {
        IMDB: "dp9861710",
        titulo: "El Lobo de Wall-Street",
        director: "Martin Scorsese",
        estreno: 2013,
        paises: ["Estados Unidos"],
        generos: ["Adult", "Comedy", "Drama"],
        calificacion: 7.633
    },
    {
        IMDB: "md2906995",
        titulo: "No respires",
        director: "Federico Álvarez",
        estreno: 2016,
        paises: ["Estados Unidos", "México"],
        generos: ["Thriller", "Horror"],
        calificacion: 6.42
    },
];

const pelicula = [];

for(let i = 0; i < misPeliculas.length; i++){
    pelicula.push(new Pelicula(misPeliculas[i]));
    console.log(pelicula[i].consultarInformacionPelicula());
}

