/**
 * Tiene por objetivo transformar los elementos 
 * de un arreglo. Toma el arreglo original y genera
 * una copia con la misma cantidad de elementos 
 * pero con todos los elementos cambiados
 * dependiendo de la función que se le
 * pase por parametro dicha 
 * función se le aplica a cada uno de los elementos
 */
const suma = (ns) => {
    let acumulado = 0;
    for(let elem of ns){
       acumulado += elem; 
    }
    return acumulado;
};
const numeros = [1, 2, 3, 4, 5];

const mascotas = [
    {nombre: "Puchini", edad: 12, tipo: "Perro"},
    {nombre: "Chanchito feliz", edad: 3, tipo: "Perro"},
    {nombre: "Pulga", edad: 10, tipo: "Perro"},
    {nombre: "Pelusa", edad: 12, tipo: "Gato"},
];

//multiplicar * 2
const numerosMultiplicados = numeros.map(x => x*2);
//a pares
const numerosParejas = numeros.map(x => [x, x]);
//edad promedio
const edades = mascotas.map(x => x.edad);
let resultado = suma(edades) / edades.length;

console.log(numeros);
console.log(numerosMultiplicados);
console.log(numerosParejas);
console.log(edades);
console.log(resultado);

