/**
 * El objetivo que tiene el método filter
 * es poder crear un arreglo de igual o menor
 * cantidad de elementos que el arreglo que 
 * nosostros estamos trabajando inicialmente.
 * Devuelve un arreglo completamente nuevo, 
 * es decir, es un método inmutable por lo que
 * el arreglo original conservará sus mismos elementos.
 */

const numeros = [1, 2, 3, 4, 5, 6, 7, 8, 9];

const mascotas = [
    {nombre: "Puchini", edad: 12, raza: "Perro"},
    {nombre: "Chanchito feliz", edad: 3, raza: "Perro"},
    {nombre: "Pulga", edad: 10, raza: "Perro"},
    {nombre: "Pelusa", edad: 16, raza: "Gato"},
];

//Filtrando los numeros menores a 5 para construir el nuevo arreglo
const numerosFiltrados = numeros.filter(x => x<5);
//Filtrando las mascotas que sean perros para construir el nuevo arreglo
const mascotasFiltradas = mascotas.filter(x => x.raza==="Perro");

//Arreglo original
console.log(numeros);
//Nuevo arreglo filtrado
console.log(numerosFiltrados);
//Arreglo original
console.log(mascotas);
//Nuevo arreglo filtrado
console.log(mascotasFiltradas);