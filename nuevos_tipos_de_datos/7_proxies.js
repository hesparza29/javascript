/**
 * Es un nuevo mecanismo 
 * que tiene JavaScript que 
 * nos permite crear un objeto 
 * basado en un objeto literal 
 * todo esto se maneja
 * mediante el handler
 * Se pueden validar los valores asignadas a 
 * las propiedades del objeto, hace una vinculación entre 
 * el objeto originbal y el objeto copia y mediante el
 * handler se validan los valores.
 */

const persona = {
    nombre: "",
    apellido: "",
    edad: 0,
    setNombre(nombre){
        this.nombre = nombre;
    },
};

const manejador = {
    set: function(obj, prop, valor){
        if(Object.keys(obj).indexOf(prop) === -1){
            return console.error(`La propiedad ${prop} no existe en el objeto persona`);
        }

        if(
            (prop==="nombre" || prop==="apellido") &&
            !(/^[A-Za-zÑñÁáÉéÍíÓóÚú\s]+$/g.test(valor))
        ){
            return console.error(`La propiedad "${prop}" sólo acepta letras y espacios en blanco`);
        }

        obj[prop] = valor;
    }
}

let hector = new Proxy(persona, manejador);
hector.nombre = "Héctor";
hector.apellido = "Esparza";
hector.edad = 26;
console.log(hector);