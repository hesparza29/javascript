/**
 * Son referencias débiles 
 * al ser referencias débiles el recolector
 * de basura elimina esas refeencias en memoria.
 * No son iterables
 * No se pueden eliminar todos los elementos en una sola instrucción
 * No tienen propiedad length o size
 */

//Para los weaksets no se pueden asignar valores directos al momento de instanciarse
const ws = new WeakSet();

//Mediante el metodo add le vamos a añadir elementos pero estos elementos tienen que ser referencias en memoria
let valor1 = {"valor1": 1};
let valor2 = {"valor2": 2};
let valor3 = {"valor3": 3};

ws.add(valor1);
ws.add(valor2);

console.log(ws);

//Verificar si un weakset cuenta con alguna entrada
console.log(ws.has(valor1));
console.log(ws.has(valor3));

//Eliminar un entrada dentro de un weakset
ws.delete(valor2);
console.log(ws);

ws.add(valor2);
ws.add(valor3);
console.log(ws);

setInterval(() => console.log(ws), 1000);

setTimeout(() => {
    valor1 = null;
    valor2 = null;
    valor3 = null;
}, 5000);