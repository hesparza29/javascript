/**
 * Un Map es un objeto dentro de JavaScript 
 * que funciona como una colección de datos, 
 * un diccionario o un mapa.
 * 
 */

//Declaración de un Map
let mapa = new Map();
//Añadir elementos a un Map ("clave", "valor")
mapa.set("nombre", "Héctor");
mapa.set("apellido", "Esparza");
mapa.set("edad", 26);

console.log(mapa);
//Obtener el número de elementos de la colección Map
console.log(mapa.size);
//Saber si un Map cuenta con una clave
console.log(mapa.has("correo"));
console.log(mapa.has("nombre"));
//Obtener el valor de una clave 
console.log(mapa.get("nombre"));
//Cambiar el valor de una clave
console.log(mapa.set("nombre", "Ing Héctor"));
console.log(mapa.get("nombre"));
//Eliminar un elemento de un Map
mapa.delete("apellido");
console.log(mapa);

//Imprimir valores de un Map mediante un ciclo
for(let [key, value] of mapa){
    console.log(`Clave:${key}, Valor:${value}`);
}

//Otra forma de inicializar Map
const mapa2 = new Map([
   ["nombre", "Daniel"],
   ["apellido", "Esparza"],
   ["ocupacion", "Ing Computación"],
   [null, "nulo"] 
]);

console.log(mapa2);

mapa2.forEach((value, key)=>{
    console.log(`Clave:${key}, Valor:${value}`);
});

//obtener las claves de un Map dentro de un arreglo
const clavesMapa2 = [...mapa2.keys()];
const valoresMapa2 = [...mapa2.values()];

console.log(clavesMapa2);
console.log(valoresMapa2);
