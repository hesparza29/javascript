/**
 * Es un tipo de dato primitivo
 * una vez que creamos un symbol se 
 * va a mantener privadas
 * Me permite crear identificadores 
 * únicos, es decir, identificadores de referencia
 * única.
 */

//Declaración de un Symbol
const NOMBRE = Symbol("nombre");
const SALUDAR = Symbol("saludar");

const persona = {
    [NOMBRE]: "Hec",
    edad: 26,
};

console.log(persona);

persona.NOMBRE = "Héctor Esparza";
console.log(persona);
console.log(persona.NOMBRE);
console.log(persona[NOMBRE]);

persona[SALUDAR] = ()=>{
    console.log("Hola");
}

console.log(persona);
persona[SALUDAR]();

for(let propiedad in persona){
    console.log(propiedad);
    console.log(persona[propiedad]);
}

//Obtener los symbolos de un objeto
console.log(Object.getOwnPropertySymbols(persona));
