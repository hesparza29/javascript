/**
 * Un objeto set es una estructura 
 * de datos parecida a un arreglo dentro de
 * JavaScript que solamente acepta 
 * valores únicos de datos primitivos.
 * 
 */

//Declaración de una estructura Set
const set = new Set([1,2,3,3,4,5,true,false,false,{},{},"hola","HOla"]);
console.log(set);
//Acceder al tamaño de una estructura Set
console.log(set.size);

//Declaración de una estructura Set
const set2 = new Set();
//Añadir valores a la estructura Set
set2.add(1);
set2.add(2);
set2.add(2);
set2.add(3);
set2.add(true);
set2.add(false);
set2.add(true);
set2.add({});

console.log(set2);
console.log(set2.size);

//Recorrer los elementos internos de una estructura Set
console.log("Recorriendo set");
set.forEach(elem => console.log(elem));
//Recorrer los elementos internos de una estructura Set
console.log("Recorriendo set2");
for (let elem of set2){
    console.log(elem);
}

/**
 * Para poder acceder a 
 * un elemento en particular dentro de una
 * estructura Set hay que transformar esa estructura Set
 * a un arreglo para que pueda ser iterable
 */
let setToArr = Array.from(set);
console.log(setToArr[0]);
console.log(setToArr[9]);
console.log(setToArr.length);

//Eliminar un elemento dentro de la estructura Set
console.log("Eliminando la entrada 'HOla' del set");
set.delete("HOla");
console.log(set);

//Buscar elementos dentro de una estructura Set
console.log("Buscando elementos dentro de una estructura Set");
console.log(`Buscar hola dentro de set ${set.has("hola")}`);
console.log(`Buscar 19 dentro de set ${set.has(19)}`);
//Vaciar el contenido de una estructura Set
set2.clear();
console.log(set2);