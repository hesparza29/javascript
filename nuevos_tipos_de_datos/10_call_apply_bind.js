/**
 * apply: Llame a un método de un objeto y reemplace el objeto actual con otro objeto. 
 * Por ejemplo: B.apply (A, argumentos); es decir, el método de un objeto A que aplica un objeto B.
 * call: Llame a un método de un objeto y reemplace el objeto actual con otro objeto. 
 * Por ejemplo: B.call (A, args1, args2); es decir, el objeto A llama al método del objeto B.
 * bind: Hace un enlace al scope en la que ha sido creada
 */

console.log(this);
this.lugar = "Contexto Global";

function saludar(saludo, aQuien){
    console.log(`${saludo} ${aQuien} desde el ${this.lugar}`);
}

saludar("Que hay de nuevo", "Bugs");

const obj = {
    lugar: "Contexto Objeto",
}

saludar.call(obj, "Hola", "Héctor");
saludar.call(null, "Hola", "Héctor");
saludar.call(this, "Hola", "Héctor");
saludar.apply(obj, ["Adios", "Ing Esparza"]);
saludar.call(null, "Hola", "Héctor");
saludar.call(this, "Hola", "Héctor");

const persona = {
    nombre: "Héctor",
    saludar: function(){
        console.log(`Hola ${this.nombre}`);
    }
}

persona.saludar();

const otraPersona = {
    saludar: persona.saludar.bind(persona),
}

otraPersona.saludar();