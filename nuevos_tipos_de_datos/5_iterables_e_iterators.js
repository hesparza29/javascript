/**
 * Una variable o un tipo de datos es iterable 
 * es una estructura de datos lineal que hacen que sus
 * elementos sean públicos y se puedan recorrer.
 * Iterable es el elemento el cual su contenido se puede recorrer
 * Iterador es el apuntador que está recorriendo los elementos
 */

//Tipos de Iterables
//const iterable = "Hola Mundo";
//const iterable = new Set([1,2,3,3,4,5]);
//const iterable = new Map([["nombre", "Héctor"], ["edad", "26"]]);
const iterable = [1,2,3,4,5];

//Accedemos al iterador del nuestra variable iterable
const iterador = iterable[Symbol.iterator]();

console.log(iterable);
console.log(iterador);

let next = iterador.next();

while(!next.done){
    console.log(next.value);
    next = iterador.next();
}

