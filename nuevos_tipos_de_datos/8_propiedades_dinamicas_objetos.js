/**
 * Mecanismo para generar de manera dinámica 
 * propiedades de un objeto
 * con ayuda de la notación de los corchetes
 */
let aleatorio = Math.round(Math.random() *100+5);
const objUsuarios = {
    propiedad: "Valor",
    [`id_${aleatorio}`]: "Valor Aleatorio"
};
console.log(objUsuarios);

const usuarios = ["Héctor", "Juan", "Fernanda", "Mike", "David"];
usuarios.forEach((usuario, index)=>objUsuarios[`id_${index}`] = usuario);

console.log(objUsuarios);