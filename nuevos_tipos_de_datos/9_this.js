/**
 * this es una palabra que hace referencia
 * al objeto en cuestión que se está trabajando.
 * Es decir dentro del scope o ambito
 * Cada función en JavaScript crea un
 * contexto excepto las arrow function 
 * que heredan el contexto de dónde son creadas
 */

console.log(this);
console.log(window);
console.log(this===window);

this.nombre = "Contexto Global";
console.log(this.nombre);

function imprimir(){
    console.log(this.nombre);
}

imprimir();

const obj = {
    nombre: "Contexto Objeto",
    imprimir: function(){
        console.log(this.nombre);
    }
}

obj.imprimir();

const obj2 = {
    nombre: "Contexto Objeto 2",
    imprimir,
}

obj2.imprimir();

const obj3 = {
    nombre: "Contexto Objeto 3",
    imprimir: ()=>{
        console.log(this.nombre);
    }
}

obj3.imprimir();

function Persona(nombre){
    this.nombre = nombre;

    return ()=>console.log(this.nombre);
}

let hector = new Persona("Héctor");
hector();