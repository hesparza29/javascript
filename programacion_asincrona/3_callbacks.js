/**
 * Una callback es una función que se pasa como parametro 
 * a otra función
 */

/**
 * Ejemplo muy tipico: procesar un array
 */

const zfigthers = ["Goku", "Vegeta", "Gohan", "Piccolo"];

zfigthers.forEach((name, index)=>{
    console.log(`${index+1}. ${name}`);
});

/** 
 * Callbacks como funciones nombradas
*/

const allUsers = [];

//Función de logeo genérica
function logStuff(data){
    if(typeof data === "string") return console.log(data);

    if(data instanceof Object){
        for (let key in data) {
            console.log(`${key}: ${data[key]}`);
        }
    }
}

//Función que recibe dos parámetros; el segundo es un callback
function getInput(input, callback){
    allUsers.push(input);

    callback(input);
}

//Llamamos a getInput con logStuff como parámetro
//Así logStuff será ejecutada callback dentro de la función getInput
getInput({user: "Héctor", speciality: "Javascript"}, logStuff);

function cuadradoCallback(value, callback){
    setTimeout(() => {
        callback(value, value*value);
    }, 0 | Math.random() * 1000);
}

//Callback hell
cuadradoCallback(0, (value, result)=>{
    console.log("Inicia Callback");
    console.log(`Callback: ${value}, ${result}`);
    cuadradoCallback(1, (value, result) =>{
        console.log(`Callback: ${value}, ${result}`);
        cuadradoCallback(2, (value, result)=>{
            console.log(`Callback: ${value}, ${result}`);
            cuadradoCallback(3, (value, result)=>{
                console.log(`Callback: ${value}, ${result}`);
                cuadradoCallback(4, (value, result)=>{
                    console.log(`Callback: ${value}, ${result}`);
                    cuadradoCallback(5, (value, result)=>{
                        console.log(`Callback: ${value}, ${result}`);
                    });
                });
            });
        });
    });
});