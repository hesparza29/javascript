/**
 * Tipos de código en javascript
 * http://latentflip.com/loupe/(Herramienta visual de ejecución de código)
 */

//1. Código síncrono bloqueante

(()=>{
    console.log("Código síncrono");
    console.log("inicio");

    function dos(){
        console.log("Dos");
    }

    function uno(){
        console.log("Uno");
        dos();
        console.log("Tres");
    }

    uno();
    console.log("Fin");
})();

console.log("***********************");

//2. Código asíncrono no bloqueante
(()=>{
    console.log("Código Asíncrono");
    console.log("Inicio");

    function dos(){
        setTimeout(function(){
            console.log("Dos");
        }, 1000);
    }

    function uno(){
        setTimeout(function(){
            console.log("Uno");
        }, 0);
        dos();
        console.log("Tres");
    }

    uno();
    console.log("Fin");
})();
