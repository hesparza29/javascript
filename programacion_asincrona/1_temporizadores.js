/**
 * Los temporizadores son una manera de ejecutar
 * código a posterior.
 * Esto se puede realizar con setTimeout o setInterval
 * La diferencia es que setTimeout solamente se va a ejecutar
 * 1 vez cuando haya transcurrido el tiempo que le hayamos asignado, 
 * mientras que setInterval se va a ejecutar cada que haya transcurrido 
 * el tiempo que le hayamos asignado.
 */

console.log("inicio");

setTimeout(() => {
    console.log("Esto es un setTimeout que se ejecuta una sóla vez");
}, 3000);

setInterval(() => {
    console.log("Esto es un setInterval que se ejecuta indefinidamente cada cierto intervalo de tiempo");
}, 1000);

//Cancelar ejecución del setTimeout
let miTimeout = setTimeout(() => {
    console.log("Esto es un setTimeout que se ejecuta una sóla vez");
}, 3000);

clearTimeout(miTimeout);

//Cancelar ejecución del setInterval
let miInterval = setInterval(() => {
    console.log("Esto es un setInterval que se ejecuta indefinidamente cada cierto intervalo de tiempo");
}, 1000);

clearInterval(miInterval);