/**
 * .insertAdjacent...
 *   .insertAdjacentElement(position, el) <-equivalente-> .appendChild
 *   .insertAdjacentHTML(position, html) <-equivalente-> .innerHTML
 *   .insertAdjacentText(position, text) <-equivalente-> .textContent
 * 
 * Posiciones...
 *   beforebegin(antes que el propio elemento)
 *   afterbegin(Justo dentro del elemento, antes de su primer elemento hijo)
 *   beforeend(Justo dentro del elemento, después de su último elemento hijo)
 *   afterend(Después del propio elemento)
 */

 const $cards = document.querySelector(".cards"),
        $newCardBeforeBegin = document.createElement("figure"),
        $newCardAfterBegin = document.createElement("figure"),
        $newCardBeforeEnd = document.createElement("figure"),
        $newCardAfterEnd = document.createElement("figure"),
        $cloneCards = $cards.cloneNode(true);

$newCardBeforeBegin.innerHTML = `
    <img src="https://placeimg.com/200/200/any" alt="Any">
    <figcaption>Any</figcaption>
`;
$newCardAfterBegin.innerHTML = `
    <img src="https://placeimg.com/200/200/any" alt="Any">
    <figcaption>Any</figcaption>
`;
$newCardBeforeEnd.innerHTML = `
    <img src="https://placeimg.com/200/200/any" alt="Any">
    <figcaption>Any</figcaption>
`;
$newCardAfterEnd.innerHTML = `
    <img src="https://placeimg.com/200/200/any" alt="Any">
    <figcaption>Any</figcaption>
`;
$newCardBeforeBegin.classList.add("card");
$newCardAfterBegin.classList.add("card");
$newCardBeforeEnd.classList.add("card");
$newCardAfterEnd.classList.add("card");

$cards.insertAdjacentElement("beforebegin", $newCardBeforeBegin);
$cards.insertAdjacentElement("afterbegin", $newCardAfterBegin);
$cards.insertAdjacentElement("beforeend", $newCardBeforeEnd);
$cards.insertAdjacentElement("afterend", $newCardAfterEnd);

/**
 * Usando el método HTML
 */

const $newCardHTML = document.createElement("figure");
let $contentCard = `
    <img src="https://placeimg.com/200/200/any" alt="Any">
    <figcaption></figcaption>
`;

$newCardHTML.classList.add("card");

$newCardHTML.insertAdjacentHTML("beforeend", $contentCard);
$newCardHTML.querySelector("figcaption").insertAdjacentText("afterbegin", "Any");
$cards.insertAdjacentElement("afterbegin", $newCardHTML);

/**
 * Utilizando más métodos
 * prepend -> como primer hijo del elemento seleccionado
 * append -> como último hijo del elemento seleccionado
 * before -> como hermano anterior del elemento seleccionado
 * after -> como hermano posterior del elemento seleccionado
 */

const $newCardHTML2 = document.createElement("figure");
let $contentCard2 = `
    <img src="https://placeimg.com/200/200/any" alt="Any">
    <figcaption></figcaption>
`;

$newCardHTML2.classList.add("card");

$newCardHTML2.insertAdjacentHTML("afterbegin", $contentCard2);
$newCardHTML2.querySelector("figcaption").insertAdjacentText("beforeend", "Any");
$cards.append($newCardHTML2);