/**
 * Los eventos son los mecanismos 
 * dentro de JavaScript que nos 
 * permite controlar las acciones 
 * del usuario y definir cierto
 * comportamiento a nuestra aplicación
 * funciones que se ejecutan con 
 * los manejadores de eventos son
 * los event-handler u observadores
 * Existen 3 maneras de definir 
 * los eventos en JavaScript
 */

function holaMundo(){
    alert("Hola Mundo");
    console.log(event);
}

const $eventoSemantico = document.getElementById("evento-semantico"),
        $eventoMultiple = document.getElementById("evento-multiple");

//Con la declaración de evento semántico solamente se le puede asignar una función
$eventoSemantico.onclick = holaMundo;
$eventoSemantico.onclick = (e)=>{
    alert("Hola Mundo Manejador de Eventos Semántico");
    console.log(e);
    console.log(event);
};

//Con la declaración de evento múltiple podemos asignar varias funciones
$eventoMultiple.addEventListener("click", holaMundo);
$eventoMultiple.addEventListener("click", (e)=>{
    alert("Hola Mundo Manejador de Eventos Múltiples");
    console.log(e);
    console.log(e.type);
    console.log(e.target);
    console.log(event);
});