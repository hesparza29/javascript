/**
 * 
 */
console.log(document.getElementsByTagName("li"));
console.log(document.getElementsByClassName("card"));
console.log(document.getElementsByName("nombre"));
console.log(document.getElementById("menu"));
/**
 * query selector viene a remplazar TagName, ClassName y Name
 */
 console.log(document.querySelector("#menu"));
 console.log(document.querySelector("a"));
 console.log(document.querySelectorAll("a"));
 document.querySelectorAll("a").forEach((el) => console.log(el));
 console.log(document.querySelector(".card"));
 console.log(document.querySelectorAll(".card"));
 console.log(document.querySelectorAll(".card")[2]);
 console.log(document.querySelector("#menu li"));
 console.log(document.querySelectorAll("#menu li"));