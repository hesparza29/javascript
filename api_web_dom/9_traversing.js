/**
 * Enfocado a los elemtnos
 * Propiedades que sirven para recorrer el DOM
 */

const $cards = document.querySelector(".cards");

console.log($cards);
console.log($cards.children);
console.log($cards.children[2]);
console.log($cards.parentElement);
console.log($cards.firstElementChild);
console.log($cards.lastElementChild);
console.log($cards.previousElementSibling);
console.log($cards.nextElementSibling);
/**
 * propiedad método nuevo
 * va a buscar el ancestro
 * más cercano del tipo 
 * que le indiquemos
 */
console.log($cards.closest("div"));
console.log($cards.closest("body"));
console.log($cards.children[3].closest("section"));