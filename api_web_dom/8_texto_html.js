/**
 * 
 */

const $whatIsDOM = document.getElementById("que-es");

let text = `
    <p>
        El Modelo de Objetos del Documento (<b><i>DOM - Document Object Model</i></b>) es un API para
        documentos HTML y XML.
    </p>
    <p>
        éste proveé una representación estructural del documento, permitiendo modificar su contenido y
        presentación visual mediante código JS.
    </p>
    <p>
        <mark>EL DOM no es parte de la especificación de JavaScript, es una API para los navegadores.</mark>
    </p>
`;

$whatIsDOM.innerText = text;
//Insertar solo texto
$whatIsDOM.textContent = text;
//Insertar solo html
$whatIsDOM.innerHTML = text;
//Reemplazar el elemento del DOM con el html que le insertes
$whatIsDOM.outerHTML = text;