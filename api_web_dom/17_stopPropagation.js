/**
 * Como evitar que se
 * propagen los eventos 
 * padres o hijos
 * dependiendo del flujo 
 * de evtnos burbuja o captura
 * preventDefaul: cancela 
 * el evento por defecto 
 * del elemento seleccionado
 */

 const $divsEventos = document.querySelectorAll(".eventos-flujo div"),
        $linkEventos = document.querySelector(".eventos-flujo a");

function flujoEventos(e){
    console.log(`Hola te saluda ${this.className}, el click lo originó ${e.target.className}`);
    e.stopPropagation();
}

$divsEventos.forEach(div=>{
    //Fase de burbuja
    div.addEventListener("click", flujoEventos);
});

$linkEventos.addEventListener("click", (e)=>{
    alert("Hola soy tu amigo y docente digital... Jonathan Mir");
    e.preventDefault();
    e.stopPropagation();
});

