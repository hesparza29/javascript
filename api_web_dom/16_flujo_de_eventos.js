/**
 * Una vez que un evento
 * se origina tiene una propagación 
 * a lo largo del arbol del DOM, por defecto
 * se va haciendo del elemento más interno hasta el 
 * más externo que es el document, a éste 
 * modelo se le llama de burbuja.
 * El tercer parametro del addEventListener 
 * puede ser un valor boolean el cual indica
 * false: fase de burbuja(elemento más interno al más externo)
 * true: fase de captura(elemento más externo al más interno)
 * Adicionalmente se pue pasar un objeto para especificar 
 * sí se quiere ejecutar solamente una vez el evento
 */

const $divsEventos = document.querySelectorAll(".eventos-flujo div");

console.log($divsEventos);

function flujoEventos(e){
    console.log(`Hola te saluda ${this.className}, el click lo originó ${e.target.className}`);
}

$divsEventos.forEach(div=>{
    //Fase de burbuja
    //div.addEventListener("click", flujoEventos);
    //div.addEventListener("click", flujoEventos, false);
    //Fase de captura
    //div.addEventListener("click", flujoEventos, true);
    div.addEventListener("click", flujoEventos, {
        capture: false,
        once: true,
    });
});