/**
 * Para ejecutar una función con 
 * parametros dentro de un evento 
 * es necesario ejecutar una función 
 * anonima o una arrow function 
 * que funjan como la función 
 * manejadora del evento, para que dentro de 
 * ella se pueda ejecutar la función que 
 * necesitemos que reciba parametros
 */
function holaMundo(){
    alert("Hola Mundo");
    console.log(e);
}

function saludar(nombre = "desconocid@"){
    alert(`Hola ${nombre}`);
}

const $eventoMultiple = document.getElementById("evento-multiple"),
        $eventoRemover = document.getElementById("evento-remover");

$eventoMultiple.addEventListener("click", holaMundo);
$eventoMultiple.addEventListener("click", ()=>{
    saludar();
    saludar("Inge Esparza");
});

//Remover un evento
const removerDobleClick = (e) => {
    alert(`Removiendo elevento de tipo ${e.type}`);
    console.log(e);
    $eventoRemover.removeEventListener("dblclick", removerDobleClick);
    $eventoRemover.setAttribute("disabled", true);
};

$eventoRemover.addEventListener("dblclick", removerDobleClick);
