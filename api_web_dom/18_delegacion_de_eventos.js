/**
 * Asignar un evento general al document 
 * y mediante condicionales 
 * identificar cual elemento es 
 * el que se le debe de delegar el evento
 * Mejora el rendimiento de la aplicación
 * No se pueden agregar addEventListener a 
 * elementos que no han sido cargados en el DOM
 * por está razón está técnica es la más adecuada.
 * 
 */
function flujoEventos(e){
    console.log(`Hola te saluda ${this}, el click lo originó ${e.target.className}`);
}

document.addEventListener("click", (e)=>{
    console.log("Click en ", e.target);

    switch(true){
        case (e.target.matches(".eventos-flujo div")):
            flujoEventos(e);
            break;
        case (e.target.matches(".eventos-flujo a")):
            alert("Hola soy tu amigo y docente digital... Jonathan Mir");
            e.preventDefault();
            break;
        default:
            break;
    }
});