/**
 * Algunos métodos del BOM
 */

// window.alert("Alerta");
// window.confirm("Confirmación");
// window.prompt("Aviso");
let ventana;

document.addEventListener("click", (e)=>{

    switch(true){
        case (e.target.matches("#abrir-ventana")):
            ventana = open("https://jonmircha.com");
            break;
        case (e.target.matches("#cerrar-ventana")):
            ventana.close();
            break;
        case (e.target.matches("#imprimir-ventana")):
            print();
            break;
        default:
            break;
    }
});