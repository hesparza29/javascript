/**
 * JSON (JavaScript Object Notation - Notación de Objetos de JavaScript) es un formato ligero de intercambio de datos.
 * Un JSON se construye con una colección de pares de nombre/valor.
 */
const json = {
    cadena: "Héctor",
    numero: 35,
    boolean: true,
    arreglo: ["correr", "programar","cocinar"],
    objeto: {
        twitter: "@hesparza29",
        email: "hector.esparza.29@gmail.com"
    },
    nulo: null
};

console.log(json);
console.log(JSON.stringify(json));

console.log(JSON.parse("{}"));
console.log(JSON.parse("[1,2,3]"));
console.log(JSON.parse("true"));
console.log(JSON.parse("false"));
console.log(JSON.parse("19"));
/**
 * no valido para parsear
 * console.log(JSON.parse("Hola Mundo"));
 */
console.log(JSON.parse("null"));
/**
 * no valido para parsear
 * console.log(JSON.parse("undefined"));
 */

 console.log(JSON.stringify({}));
 console.log(JSON.stringify([1,2,3]));
 console.log(JSON.stringify(true));
 console.log(JSON.stringify(false));
 console.log(JSON.stringify(19));
 console.log(JSON.stringify(null));

 console.log(JSON.parse(`{
    "cadena": "Héctor",
    "numero": 35,
    "boolean": true,
    "arreglo": ["correr", "programar","cocinar"],
    "objeto": {
        "twitter": "@hesparza29",
        "email": "hector.esparza.29@gmail.com"
    },
    "nulo": null
}`));