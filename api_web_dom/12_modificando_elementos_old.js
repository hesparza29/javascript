/**
 * Métodos para remplazar 
 * poner al inicio o 
 * en una posición especifica dentro 
 * del DOM
 */

const $cards = document.querySelector(".cards"),
        $newCard = document.createElement("figure"),
        $cloneCards = $cards.cloneNode(true);

$newCard.innerHTML = `
    <img src="https://placeimg.com/200/200/any" alt="Any">
    <figcaption>Any</figcaption>
`;
$newCard.classList.add("card");
//Remplazar un nodo hijo del selector elegido
//$cards.replaceChild($newCard, $cards.children[2]);
//Remplazar un nodo en una posición especifica
//$cards.insertBefore($newCard, $cards.firstElementChild);
//Eliminar un nodo de un selector elegido
//$cards.removeChild($cards.lastElementChild);
document.body.appendChild($cloneCards);