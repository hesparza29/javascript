/**
 * Crear elementos dinámicamente
 * con JavaScript. Es decir, 
 * agregar etiquetas html
 */

//Creando elementos html
const $figure = document.createElement("figure"),
        $image = document.createElement("img"),
        $figcaption = document.createElement("figcaption"),
        $figcaptionText = document.createTextNode("Animals"),
        $cards = document.querySelector(".cards");

$image.src = "https://placeimg.com/200/200/animals";
$image.setAttribute("alt", "Animals");
$figcaption.appendChild($figcaptionText);
$figure.appendChild($image);
$figure.appendChild($figcaption);
//Añadiendo las clases a la tarjeta
$figure.classList.toggle("card");
$cards.appendChild($figure);

//Otra forma de crear html dinámico(con está forma JavaScript no lo considera un nodo)
const $figure2 = document.createElement("figure");
$figure2.innerHTML = `
    <img src="https://placeimg.com/200/200/people" alt="People" />
    <figcaption>People</figcaption>
`;
$figure2.classList.add("card");
$cards.appendChild($figure2);

//Agregar varios nodos dinámicamente 
const estaciones = ["Primavera", "Verano", "Otoño", "Invierno"],
        $ul = document.createElement("ul");
document.write("<h3>Estaciones del Año</h3>");

estaciones.forEach(el =>{
    const $li = document.createElement("li");
    $li.textContent = el;
    $ul.appendChild($li);
});

document.body.appendChild($ul);

/**
 * Añadiendo elementos al DOM
 * con la técnica de fragmentos esto 
 * para mejorar el rendimiento ya que 
 * genera menos procesamiento
 */

const meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
                "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        $ul2 = document.createElement("ul"), 
        $fragment = document.createDocumentFragment();

meses.forEach(el =>{
    const $li = document.createElement("li");
    $li.textContent = el;
    $fragment.appendChild($li);
});

document.write("<h3>Meses del Año</h3");
$ul2.appendChild($fragment);
document.body.appendChild($ul2);