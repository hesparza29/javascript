/**
 * Manejo del BOM: Browser Object Model
 * Soneventos que cuelgan directamente del objeto window
 */

window.addEventListener("resize", (e)=>{
    console.clear();
    console.log("*****Evento resize*****");
    console.log(window.innerWidth);
    console.log(window.innerHeight);
    console.log(window.outerWidth);
    console.log(window.outerHeight);
    console.log(e);
});


window.addEventListener("scroll", (e)=>{
    console.clear();
    console.log("*****Evento scroll*****");
    console.log(window.scrollX);
    console.log(window.scrollY);
    console.log(e);
});


window.addEventListener("load", (e)=>{
    console.log("*****Evento Load");
    console.log(window.screenX);
    console.log(window.screenY);
    console.log(e);
});

/**
 * Más eficiente para peticiones asincronas
 * Equivalente al $(document).ready() de jquery
 */
document.addEventListener("DOMContentLoaded", (e)=>{
    console.log("*****Evento DOMContentLoaded");
    console.log(window.screenX);
    console.log(window.screenY);
    console.log(e);
});