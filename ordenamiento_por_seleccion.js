/**
 * Algotimo de ordenamiento por seleccion
 */

export const ordenamientoPorSeleccion = (arreglo) => {

    for(let i=0 ; i < arreglo.length-1 ; i++){

        let menor = i;
    
        for(let j=i+1 ; j<arreglo.length; j++){
            (arreglo[menor]>arreglo[j]) ? menor = j : false;
        }
        let auxiliar = arreglo[menor];
        arreglo[menor] = arreglo[i];
        arreglo[i] = auxiliar;
    }

    return arreglo;
};
